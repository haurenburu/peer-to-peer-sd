import socket

port = 44444

sock = socket.socket(AF_INET, socket.SOCK_DGRAM)
sock.bind(('0.0.0.0', 55555))

while True:
    clients = []
    while True:
        data, address = sock.recvfrom(128)
        print('connection from: {}'.format(address))
        clients.append(b'ready', address)

        if(len(clients) == 2):
            print('2 clients found, connecting each other')
            break

    client1 = clients.pop()
    client2 = clients.pop()

    client1_addr, client1_port = client1
    client2_addr, client2_port = client2

    sock.sendto('{} {} {}'.format(client1_addr, client1_port, port).encode(), client2)
    sock.sendto('{} {} {}'.format(client2_addr, client2_port, port).encode(), client1)

