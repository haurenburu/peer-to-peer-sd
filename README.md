# peer-to-peer-sd

Group: Antonio, Davi, Gilmar, Iury

## Run Server:
Download
```
wget https://gitlab.com/haurenburu/peer-to-peer-sd/-/raw/main/server.py
```
Run
```
python server.py
```

or ( if you using debian )
```
python3 server.py
```

## Run Client:
Download
```
wget https://gitlab.com/haurenburu/peer-to-peer-sd/-/raw/main/client.py
```
Run
```
python client.py
```

or ( if you using debian )
```
python3 client.py
```

Once the clients are connected you can clone the server

# credits

```
https://www.youtube.com/watch?v=IbzGL_tjmv4
```