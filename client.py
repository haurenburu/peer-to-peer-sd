import socket
import sys
import threading

serverIp = input('ServerIP: ')


server = (serverIp, 55555)

print('connecting to', serverIp)


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('0.0.0.0', 33333))
sock.sendto(b'0', server)

while True:
    data = sock.recv(1024).decode()

    if(data.strip() == 'ready'):
        print('Waiting for the other client')
        break

data = sock.recv(1024).decode()
ip, sport, dport = data.split(' ')
sport = int(sport)
dport = int(dport)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('0.0.0.0', sport))
sock.sendto(b'0', (ip, dport))

print("Connected to another client")

# wait for messages
def listen():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('0.0.0.0', sport))

    while True:
        data = sock.recv(1024)
        print('\rPEER: {}\n>'.format(data.decode()), end='')

listener = threading.Thread(target=listen, daemon=True)
listener.start()
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('0.0.0.0', dport))
# send messages
while True:
    msg = input('> ')
    sock.sento(msg.encode(), (ip, sport))
